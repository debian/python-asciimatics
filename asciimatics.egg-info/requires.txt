pyfiglet>=0.7.2
Pillow>=2.7.0
wcwidth
future

[:sys_platform == "win32"]
pypiwin32
